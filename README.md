# Embedded Annotations Datasets #

To verify and evaluate the development/usage of embedded annotations, a collection of annotated projects have been created within easelab. All our projects with embedded annotations follow the specification
of embedded annotations [Link](https://bitbucket.org/easelab/faxe/src/master/specification/embedded_annotation_specification.pdf) 

## Dataset Bitcoin-wallet ##
[https://bitbucket.org/easelab/datasetbitcoinwallet](https://bitbucket.org/easelab/datasetbitcoinwallet)

## Dataset ClaferConfigurator ##
[https://bitbucket.org/easelab/datasetclaferconfigurator](https://bitbucket.org/easelab/datasetclaferconfigurator)

## Dataset ClaferIDE ##
[https://bitbucket.org/easelab/datasetclaferide](https://bitbucket.org/easelab/datasetclaferide)

## Dataset ClaferMooVisualizer ##
[https://bitbucket.org/easelab/datasetclafermoovisualizer](https://bitbucket.org/easelab/datasetclafermoovisualizer)

## Dataset ClaferToolsUICommonPlatform ##
[https://bitbucket.org/easelab/datasetclafertoolsuicommonplatform](https://bitbucket.org/easelab/datasetclafertoolsuicommonplatform)

## Dataset Marlin ##
[https://bitbucket.org/easelab/datasetmarlin](https://bitbucket.org/easelab/datasetmarlin)

## Dataset Snake ##
[https://bitbucket.org/easelab/datasetsnake](https://bitbucket.org/easelab/datasetsnake)

## Tool FAXE ##
[https://bitbucket.org/easelab/faxe](https://bitbucket.org/easelab/faxe) 